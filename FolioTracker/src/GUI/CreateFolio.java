package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Listener.AddButton;
import Listener.BuyButton;
import Listener.RmButton;
import Listener.SellButton;

public class CreateFolio {
	
	private static final Color TABCOL = new Color(169,193,201);
	protected static JTable table;
	
	public CreateFolio() {
		try {
			String name = JOptionPane.showInputDialog(null,"Enter folio name?");
			if(!name.isEmpty()){
				if(TabPanel.myList.contains(name)){
					JOptionPane.showMessageDialog(null,
							name + " already in use.",
							"Error",
							JOptionPane.ERROR_MESSAGE);
				}
				else
				{
					newFolio(name);
				}
			}
			else
			{
				JOptionPane.showMessageDialog(null,
						"Folio must have a name.",
						"Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
		catch(RuntimeException e) {
		}
	}
	
	
	private void newFolio(String name){
		JPanel tabPanel = new JPanel(new BorderLayout());
		tabPanel.setBackground(TABCOL);

		TabPanel.myList.add(name);
		
		
		JPanel topPane = new JPanel(new FlowLayout(FlowLayout.LEADING,20,5));
		topPane.setPreferredSize(new Dimension(500, 500/10));
		topPane.setBackground(TABCOL);

		JButton addButton = new JButton("Add");
		JButton rmButton = new JButton("Delete");
		JButton buyButton = new JButton("Buy");
		JButton sellButton = new JButton("Sell");
		addButton.addActionListener(new AddButton());
		rmButton.addActionListener(new RmButton());
		buyButton.addActionListener(new BuyButton());
		sellButton.addActionListener(new SellButton());
		topPane.add(addButton);
		topPane.add(rmButton);
		topPane.add(buyButton);
		topPane.add(sellButton);

		tabPanel.add(topPane,BorderLayout.PAGE_START);


		String[] columnNames= {"Ticker Symbol", "Stock Name", "Number of Shares", 
				"Price per Share", "Value of Holding", "Select"};

		Object[][] dataValues= {	
				{"TST1", "Test Stock 1", 100, 1.0,100.0,new Boolean(false)},
				{"TST2", "Test Stock 2", 200, 2.0,400.0,new Boolean(false)},
				{"TST3", "Test Stock 3", 300, 3.0,900.0,new Boolean(false)}
		};
		
		
		DefaultTableModel model = new DefaultTableModel(dataValues, columnNames);
		table = new JTable(model) {
			/**
			 * 
			 */


			private static final long serialVersionUID = 1L;

			@Override
			public Class getColumnClass(int column) {
				switch (column) {
				case 0:
					return String.class;
				case 1:
					return String.class;
				case 2:
					return Integer.class;
				case 3:
					return Double.class; 
				case 4:
					return Double.class;
				default:
					return Boolean.class;
				}
			}
		};
		
		

		double current,nCurrent = 0;

		for(int i = 0; i < dataValues.length;)
		{
			current = (double) dataValues[i++][4];
			nCurrent = current + nCurrent;
		}

		table.setFillsViewportHeight(true);

		MouseListener mouseListener = null;
		table.addMouseListener(mouseListener);		
		JScrollPane scrollPane = new JScrollPane(table);
		tabPanel.add(scrollPane);

		JPanel bottomPane = new JPanel(new FlowLayout(FlowLayout.CENTER));
		bottomPane.setPreferredSize(new Dimension(500, 500/10));
		bottomPane.setBackground(TABCOL);

		JLabel totalLabel = new JLabel("Folio Name " + name + " - Total Value " + nCurrent);
		bottomPane.add(totalLabel);

		tabPanel.add(bottomPane,BorderLayout.PAGE_END);
		TabPanel.tabPane.addTab(name,tabPanel);
	}
}
