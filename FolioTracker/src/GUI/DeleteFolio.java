package GUI;

import javax.swing.JOptionPane;

public class DeleteFolio {
	
	public DeleteFolio() {
		try {
			String name = JOptionPane.showInputDialog(null,"Enter folio name that you wish to delete?");
			if(!name.isEmpty()){
				if(TabPanel.myList.contains(name)) {
					int reply = JOptionPane.showConfirmDialog(null, 
							"Are you sure you want to delete " + name, 
							"Warning", 
							JOptionPane.YES_NO_OPTION);
					if (reply == JOptionPane.YES_OPTION) {
						TabPanel.tabPane.removeTabAt(TabPanel.myList.indexOf(name));
						TabPanel.myList.remove(name);
					}
				}
				else {
					JOptionPane.showMessageDialog(null,
							name + " is not a folio.",
							"Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
			else
			{
				JOptionPane.showMessageDialog(null,
						"Enter a valid folio to delete",
						"Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
		catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"No folio to delete.",
					"Error",
					JOptionPane.ERROR_MESSAGE);
		}
	}
}
