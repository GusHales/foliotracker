package GUI;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import Listener.DeleteFolioAction;
import Listener.ExitAction;
import Listener.NewFolioAction;
import Listener.OpenAction;
import Listener.SaveAction;
import Model.IFolioTracker;

public class GUIMenuBar extends JMenuBar{
	
	public GUIMenuBar(TabPanel gui){
		JMenu menu = new JMenu("File");
		JMenuItem menuNewFolio = new JMenuItem("New Folio");
		JMenuItem menuDeleteFolio = new JMenuItem("Delete Folio");
		JMenuItem menuOpen = new JMenuItem("Open");
		JMenuItem menuSave = new JMenuItem("Save");
		JMenuItem menuExit = new JMenuItem("Exit");

		//New Folio Action for menu bar
		menuNewFolio.addActionListener(new NewFolioAction(gui));
		//New Folio Action for menu bar
		menuDeleteFolio.addActionListener(new DeleteFolioAction());
		//Open Action for menu bar
		menuOpen.addActionListener(new OpenAction());
		//Save Action for menu bar
		menuSave.addActionListener(new SaveAction());
		//Exit Action for menu bar
		menuExit.addActionListener(new ExitAction());
		menu.add(menuNewFolio);
		menu.add(menuDeleteFolio);
		menu.add(menuOpen);
		menu.add(menuSave);
		menu.add(menuExit);
		this.add(menu);
	}


}
