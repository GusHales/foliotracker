package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.*;

import Model.FolioTracker;
import Model.IFolioTracker;

public class MainFrame extends JFrame{
	private static final int FRAME_HEIGHT = 500;
	private static final int FRAME_WIDTH = 700;
	private static final Color BACKGROUNDCOL = new Color(109,142,154);
	protected JPanel contentPane;
	
	public MainFrame(){
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setTitle("Folio Tracker");
		this.setMinimumSize(new Dimension(FRAME_WIDTH,FRAME_HEIGHT));

		contentPane = new JPanel(new BorderLayout());
		contentPane.setBackground(BACKGROUNDCOL);
		this.setContentPane(contentPane);

	}
}
