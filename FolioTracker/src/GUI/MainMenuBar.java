package GUI;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import Listener.DeleteFolioAction;
import Listener.ExitAction;
import Listener.CreateFolioAction;
import Listener.SaveAction;

public class MainMenuBar extends JMenuBar{
	
	public MainMenuBar(MainFrame gui){
		JMenu menu = new JMenu("File");
		JMenuItem menuNewFolio = new JMenuItem("New Folio");
		JMenuItem menuDeleteFolio = new JMenuItem("Delete Folio");
		JMenuItem menuSave = new JMenuItem("Save");
		JMenuItem menuExit = new JMenuItem("Exit");

		//New Folio Action for menu bar
		menuNewFolio.addActionListener(new CreateFolioAction());
		//New Folio Action for menu bar
		menuDeleteFolio.addActionListener(new DeleteFolioAction());
		//Save Action for menu bar
		menuSave.addActionListener(new SaveAction());
		//Exit Action for menu bar
		menuExit.addActionListener(new ExitAction());
		menu.add(menuNewFolio);
		menu.add(menuDeleteFolio);
		menu.add(menuSave);
		menu.add(menuExit);
		this.add(menu);
		gui.setJMenuBar(this);
	}


}
