package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import Listener.AddStock;
import Listener.CloseStock;
import Model.IPortfolio;

public class NewStock {

	private static final Color TABCOL = new Color(169,193,201);
	private static final Color BACKGROUNDCOL = new Color(109,142,154);
	private JTextField textFieldSymbol, textFieldNum;
	private IPortfolio folio;
	private DefaultTableModel model;
	private JFrame frame;

	public NewStock(IPortfolio folio, DefaultTableModel model) {
		this.folio = folio;
		this.model = model;
		addNewStock();
	}

	private void addNewStock(){
		frame= new JFrame("New Stock");

		JPanel textPanel = new JPanel(new FlowLayout(FlowLayout.LEFT,20,20));
		textPanel.setPreferredSize(new Dimension(300, 180));
		textPanel.setBackground(TABCOL);

		JLabel tickerSymbol = new JLabel("Ticker Symbol");
		JLabel valueOfHolding = new JLabel("Number of Shares");
		textFieldSymbol = new JTextField(10);
		textFieldNum = new JTextField(10);

		textPanel.add(tickerSymbol,new FlowLayout(FlowLayout.LEFT,25,20));
		textPanel.add(textFieldSymbol, new FlowLayout(FlowLayout.RIGHT,30,20));
		textPanel.add(valueOfHolding, new FlowLayout(FlowLayout.RIGHT,20,20));
		textPanel.add(textFieldNum, new FlowLayout(FlowLayout.RIGHT,20,20));

		JPanel buttonPane = new JPanel(new FlowLayout(FlowLayout.CENTER,50,50));
		buttonPane.setPreferredSize(new Dimension(300, 100));
		buttonPane.setBackground(BACKGROUNDCOL);

		JButton addButton = new JButton("Add");
		JButton closeButton = new JButton("Close");
		addButton.addActionListener(new AddStock(folio, this, frame, model));
		closeButton.addActionListener(new CloseStock(frame));
		buttonPane.add(addButton);
		buttonPane.add(closeButton);

		frame.add(textPanel,BorderLayout.PAGE_START);
		frame.add(buttonPane,BorderLayout.PAGE_END);
		frame.setPreferredSize(new Dimension(300, 300));
		frame.pack();
		frame.setVisible(true);
		frame.setResizable(false);
	}

	public void errorMesPopUp(String mes){
		JOptionPane.showMessageDialog(frame, mes);
		this.textFieldNum.setText("");
		this.textFieldSymbol.setText("");
	}

	public String getNumber(){
		return textFieldNum.getText();

	}
	public String getSymbol(){
		return textFieldSymbol.getText();

	}
}
