package GUI;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class Save {
	private static JFileChooser fileChooser = new JFileChooser();
	public Save(){
		int returnVal = fileChooser.showDialog(null, "Save");
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			try {
				File file = fileChooser.getSelectedFile();
				PrintWriter folioData = new PrintWriter(file);
				folioData.println();
				folioData.println("");

				for (int row = 0; row < CreateFolio.table.getRowCount(); row++) {
					for (int col = 0; col < CreateFolio.table.getColumnCount(); col++) {
						folioData.print(CreateFolio.table.getColumnName(col));
						folioData.print(": ");
						folioData.println(CreateFolio.table.getValueAt(row, col));
					}
					folioData.println("");
				}
				folioData.close();
				JOptionPane.showConfirmDialog(null, 
						"File has been saved in " + file, 
						"Message", 
						JOptionPane.PLAIN_MESSAGE);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
