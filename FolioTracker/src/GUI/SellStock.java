package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import Listener.CloseStock;
import Listener.Sell;
import Model.IPortfolio;

public class SellStock {

	private static final Color TABCOL = new Color(169,193,201);
	private static final Color BACKGROUNDCOL = new Color(109,142,154);
	private JTextField textFieldSymbol, textFieldHolding;
	private DefaultTableModel model;
	private JTable table;
	
	public void sellStock(IPortfolio folio, DefaultTableModel model, JTable table) {
		this.table = table;
		this.model = model;
        JFrame frame = new JFrame("New Stock");
        
        JPanel textPanel = new JPanel(new FlowLayout(FlowLayout.LEFT,20,20));
        textPanel.setPreferredSize(new Dimension(300, 180));
        textPanel.setBackground(TABCOL);
        
        JLabel valueOfHolding = new JLabel("Number of Shares");
        textFieldHolding = new JTextField(10);
        
		textPanel.add(valueOfHolding, new FlowLayout(FlowLayout.RIGHT,20,20));
		textPanel.add(textFieldHolding, new FlowLayout(FlowLayout.RIGHT,20,20));
		
		JPanel buttonPane = new JPanel(new FlowLayout(FlowLayout.CENTER,50,50));
		buttonPane.setPreferredSize(new Dimension(300, 100));
		buttonPane.setBackground(BACKGROUNDCOL);

		JButton addButton = new JButton("Sell");
		JButton closeButton = new JButton("Close");
		addButton.addActionListener(new Sell(folio, this, frame, model, table.getSelectedRow()));
		closeButton.addActionListener(new CloseStock(frame));
		buttonPane.add(addButton);
		buttonPane.add(closeButton);

		frame.add(textPanel,BorderLayout.PAGE_START);
		frame.add(buttonPane,BorderLayout.PAGE_END);
		frame.setPreferredSize(new Dimension(300, 300));
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);
	}
	
	public String getNumber(){
		return textFieldHolding.getText();
		
	}
	public String getSymbol(){
		return (String)model.getValueAt(table.getSelectedRow(), 0);
		
	}
}
