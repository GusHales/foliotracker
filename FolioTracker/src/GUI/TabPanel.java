package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import Listener.AddButton;
import Listener.SellButton;
import Listener.TableMouseListener;
import Model.DBManage;
import Model.FolioTracker;
import Model.IFolioTracker;
import Model.IPortfolio;
import Model.IStock;

public class TabPanel extends MainFrame{

	private static final Color TABCOL = new Color(169,193,201);
	public static JTabbedPane tabPane;
	private static IFolioTracker tracker;


	public TabPanel(){
		tabPane = new JTabbedPane();
		tabPane.setTabPlacement(JTabbedPane.TOP);
		tabPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		tabPane.setBackground(TABCOL);
		UIManager.put("TabbedPane.selected", TABCOL);
		tabPane.setVisible(true);
		contentPane.add(tabPane);
		contentPane.revalidate();
		contentPane.repaint();
		if(DBManage.hasFolio()){
			tracker = new FolioTracker(DBManage.getFolios());
			setTabs();
		}else{
			tracker = new FolioTracker();
		}

		this.setJMenuBar(new GUIMenuBar(this));
		this.pack();
	}

	private void setTabs() {
		for(IPortfolio port: tracker.getFolios()){
			newFolio(port.getName());
		}
	}

	public void newFolio(String name){
		JPanel tabPanel = new JPanel(new BorderLayout());
		tabPanel.setBackground(TABCOL);
		


		JPanel topPane = new JPanel(new FlowLayout(FlowLayout.LEADING,20,5));
		topPane.setPreferredSize(new Dimension(500, 500/10));
		topPane.setBackground(TABCOL);


		tabPanel.add(topPane,BorderLayout.PAGE_START);


		String[] columnNames= {"Ticker Symbol", "Stock Name", "Number of Shares", 
				"Price per Share", "Value of Holding"};
		IPortfolio p = tracker.getPortfolio(name);
		Object[][] dataValues= {};
		if(p!=null){
			
			 dataValues = new Object[p.getStockCount()][5];
			 int i=0;
			 for(IStock s : p.getStocks()){
				dataValues[i][0] = s.getTickerSymbol(); 
				dataValues[i][1] = s.getName(); 
				dataValues[i][2] = s.getNumShares(); 
				dataValues[i][3] = s.getSharePrice(); 
				dataValues[i][4] = s.getValueHolding(); 
				i++;
			 }
			
		}else{
			tracker.addFolio(name);
		}

		
		DefaultTableModel model = new DefaultTableModel(dataValues, columnNames){
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column){  
		          return false;  
		      }
		};
		JTable table = new JTable(model) {

			/**
			 * 
			 */


			private static final long serialVersionUID = 1L;

			@Override
			public Class getColumnClass(int column) {
				switch (column) {
				case 0:
					return String.class;
				case 1:
					return String.class;
				case 2:
					return Integer.class;
				case 3:
					return Double.class; 
				case 4:
					return Double.class;
				default:
					return Boolean.class;
				}
			}
		};

		table.addMouseListener(new TableMouseListener(table,this));
		JButton addButton = new JButton("Buy");
		JButton rmButton = new JButton("Sell");
		addButton.addActionListener(new AddButton(tracker, name, model));
		table.getRowSelectionAllowed();
		rmButton.addActionListener(new SellButton(name,tracker, table, model));
		topPane.add(addButton);
		topPane.add(rmButton);

		double current,nCurrent = 0;

		for(int i = 0; i < dataValues.length;)
		{
			current = (double) dataValues[i++][4];
			nCurrent = current + nCurrent;
		}

		table.setFillsViewportHeight(true);

		MouseListener mouseListener = null;
		table.addMouseListener(mouseListener);		

		JScrollPane scrollPane = new JScrollPane(table);
		tabPanel.add(scrollPane);

		JPanel bottomPane = new JPanel(new FlowLayout(FlowLayout.CENTER));
		bottomPane.setPreferredSize(new Dimension(500, 500/10));
		bottomPane.setBackground(TABCOL);
		IPortfolio folio = tracker.getPortfolio(name);		
		JLabel totalLabel = new JLabel("Folio Name " + name + " - Total Value " + folio.getTotalVal());
		bottomPane.add(totalLabel);
		System.out.println("WOOOOOOOOOOOOOO " + folio.getTotalVal());
		tabPanel.add(bottomPane,BorderLayout.PAGE_END);

		tabPane.addTab(name,tabPanel);
	}
	
	public void popUpMenu(){
		JPopupMenu pop = new JPopupMenu();
		JMenuItem editMenuItem = new JMenuItem("Edit");
		
	}

	public static void removeFolio() {
		int current = tabPane.getSelectedIndex();
		tracker.rmFolio(tabPane.getTitleAt(current));
		tabPane.remove(current);
	}
}
