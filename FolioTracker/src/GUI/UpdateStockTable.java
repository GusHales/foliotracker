package GUI;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;

import Model.DBManage;
import Model.IPortfolio;
import Model.IStock;
import Model.Stock;

public class UpdateStockTable implements Observer {

	private IStock stock;
	private JFrame frame;
	private DefaultTableModel model;
	private NewStock s;
	private IPortfolio folio;
	private int numShares;
	private double valueHolding;

	public UpdateStockTable(IStock current, JFrame frame,
			DefaultTableModel model, IPortfolio folio, NewStock s) {
		stock = current;
		stock.addObserver(this);
		this.s = s;
		this.frame = frame;
		this.model = model;
		this.folio = folio;
		numShares = stock.getNumShares();
		valueHolding = stock.getValueHolding();

		updateTable();

	}

	private void updateTable(){
		boolean found = false;
		for(int i=0; i<model.getRowCount();i++){

			if(((String)model.getValueAt(i, 0)).equals(s.getSymbol())){
				System.out.println(i);
				model.setValueAt(numShares, i, 2);
				model.setValueAt(valueHolding, i, 4);
				DBManage.updateStock(folio, stock);
				found = true;
			}
		}
		if(!found){
			model.addRow(new Object[] {stock.getTickerSymbol(), stock.getName(), 
					numShares, stock.getSharePrice(), valueHolding});
			DBManage.writeStock(folio,stock );
		}
		
		frame.dispose();
	}


	@Override
	public void update(Observable o, Object arg) {
		System.out.println("In update method");
		numShares = stock.getNumShares();
		System.out.println("num shares: " + numShares);
		valueHolding = stock.getValueHolding();
		System.out.println("val hold: " + valueHolding);
		updateTable();

	}

}
