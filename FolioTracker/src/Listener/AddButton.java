package Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.table.DefaultTableModel;

import GUI.NewStock;
import Model.FolioTracker;
import Model.IFolioTracker;

public class AddButton implements ActionListener {

	NewStock stock;
	IFolioTracker tracker;
	String name;
	DefaultTableModel model;

	public AddButton(IFolioTracker tracker, String name, DefaultTableModel model){
		this.tracker = tracker;
		this.name = name;
		this.model = model;
	}
	
	public void actionPerformed(ActionEvent event) {
		new NewStock(tracker.getPortfolio(name), model);
	}
}
