package Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;

import GUI.NewStock;
import GUI.UpdateStockTable;
import Model.DBManage;
import Model.IPortfolio;
import Model.IStock;
import Model.Portfolio;
import Model.Stock;

public class AddStock implements ActionListener {

	private IPortfolio folio;
	private NewStock s;
	private JFrame frame;
	private DefaultTableModel model;

	public AddStock(IPortfolio folio, NewStock s, JFrame frame, DefaultTableModel model){
		this.folio = folio;
		this.s = s;
		this.frame = frame;
		this.model = model;
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		IStock current;
		int sGetNum;
		try { 
			sGetNum = Integer.parseInt(s.getNumber()); 
		} catch(NumberFormatException e) { 
			sGetNum = -1;
		}

		if(s.getSymbol().isEmpty()){
			s.errorMesPopUp("Enter ticker symbol");
		}
		else {
			current = new Stock(s.getSymbol());

			if(!current.getIsValid()){	//Ticker symbol wont be recoginised
				s.errorMesPopUp("Ticker symbol not recognised!");
			}
			else if(s.getNumber().isEmpty()){
				s.errorMesPopUp("Enter number of shares");
			}
			else if(sGetNum<0){
				s.errorMesPopUp("Number of shares: incorrect format");
			}
			else if(folio.getStock(current.getTickerSymbol())!=null){
				current = folio.getStock(current.getTickerSymbol());
				current.addShares(Integer.parseInt(s.getNumber()));
				new UpdateStockTable(current, frame, model, folio, s);
			}
			else{
				current.addShares(Integer.parseInt(s.getNumber()));
				folio.addStock(current);
				new UpdateStockTable(current, frame, model, folio, s);
			}
		}
	}

}
