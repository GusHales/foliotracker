package Listener;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import GUI.Close;


public class CloseApplicationAction implements WindowListener {

	public void windowClosing(WindowEvent e) {
		new Close();
	}

	public void windowActivated(WindowEvent e) {
	}

	public void windowClosed(WindowEvent e) {
	}

	public void windowDeactivated(WindowEvent e) {
	}

	public void windowDeiconified(WindowEvent e) {
	}

	public void windowIconified(WindowEvent e) {
	}

	public void windowOpened(WindowEvent e) {
	}
}
