package Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import GUI.NewStock;

public class CloseStock implements ActionListener {

	private JFrame frame;

	public CloseStock(JFrame frame){
		this.frame = frame;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		frame.dispose();
		
	}

}
