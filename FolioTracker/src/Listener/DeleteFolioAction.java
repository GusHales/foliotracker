package Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import GUI.TabPanel;

public class DeleteFolioAction implements ActionListener{

	public void actionPerformed(ActionEvent event) {
		try {
			int reply = JOptionPane.showConfirmDialog(null, 
					"Are you sure you want to delete this folio.", 
					"Warning", 
					JOptionPane.YES_NO_OPTION);
	        if (reply == JOptionPane.YES_OPTION) {
	        TabPanel.removeFolio();	
	        }
		}
		catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"No folio to delete.",
					"Error",
					JOptionPane.ERROR_MESSAGE);
		}
	}
}