package Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import GUI.TabPanel;

public class NewFolioAction implements ActionListener{

	private TabPanel tb;
	public NewFolioAction(TabPanel gui){
		tb = gui;
	}
	public void actionPerformed(ActionEvent event) {
		try {
			String name = JOptionPane.showInputDialog(null,"Enter folio name?");
			if(!name.isEmpty()){
				tb.newFolio(name);	
			}
			else
			{
				JOptionPane.showMessageDialog(null,
						"Folio must have a name.",
						"Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
		catch(RuntimeException e) {
		}
	}
}
