package Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import GUI.CloseFrame;
import GUI.NewStock;

public class NewStockCloseButton implements ActionListener{

	public void actionPerformed(ActionEvent e) {
		new CloseFrame();
	}

}
