package Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;


public class SaveAction implements ActionListener {
	private static JFileChooser fileChooser = new JFileChooser();
	public void actionPerformed(ActionEvent event) {
		fileChooser.setMultiSelectionEnabled(true);
		fileChooser.setCurrentDirectory(new File("C:\\Documents"));
		fileChooser.showDialog(null, "Save");
    }
}
