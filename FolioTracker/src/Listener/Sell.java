package Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;

import GUI.SellStock;
import Model.IPortfolio;
import Model.IStock;
import Model.Stock;

public class Sell implements ActionListener {

	private IPortfolio folio;
	private SellStock stock;
	private JFrame frame;
	private DefaultTableModel model;
	private int row;

	public Sell(IPortfolio folio, SellStock stock, JFrame frame, DefaultTableModel model, int row){
		this.folio = folio;
		this.stock = stock;
		this.frame = frame;
		this.model = model;
		this.row = row;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
			IStock istock = new Stock(stock.getSymbol());
			istock.addShares(Integer.parseInt(stock.getNumber()));
			
			folio.addStock(istock);
			model.removeRow(row);
			frame.dispose();
		
	}

}
