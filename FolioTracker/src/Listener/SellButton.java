package Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import GUI.SellStock;
import Model.IFolioTracker;

public class SellButton implements ActionListener {

	SellStock stock = new SellStock();
	IFolioTracker tracker;
	JTable table;
	String name;
	DefaultTableModel model;

	public SellButton(String name, IFolioTracker tracker, JTable table, DefaultTableModel model){
		this.tracker = tracker;
		this.name = name;
		this.table = table;
		this.model = model;
	}
	
	public void actionPerformed(ActionEvent event) {
		stock.sellStock(tracker.getPortfolio(name), model, table);
	}
}
