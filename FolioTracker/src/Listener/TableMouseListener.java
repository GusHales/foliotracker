package Listener;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTable;
import javax.swing.SwingUtilities;

import GUI.TabPanel;

public class TableMouseListener extends MouseAdapter{

	private JTable table;
	private TabPanel tabP;
	public TableMouseListener(JTable t, TabPanel tabP){
		table = t;
		this.tabP = tabP;
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		if(SwingUtilities.isRightMouseButton(e)){
			//int index = table.rowAtPoint(e.getPoint());
			//table.getSelectionModel().set
			tabP.popUpMenu();
			
		}
		
	}


}
