package Model;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

/**
 * Class to handle interactions with a database. 
 * Connects to a remote database 
 * 
 *
 * 
 */
public class DBManage {

	/* Database credentials */
	private final static String hostname = "jdbc:mysql://199.167.30.50/folio_tracker";
	private final static String user = "root";
	private final static String pass = "rabble";

	public static void writeFolio(IPortfolio port) {
		String folioQuery = String.format("INSERT INTO folio VALUES('%s')",
				port.getName());

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			// connect to the database
			Connection conn = DriverManager.getConnection(hostname, user, pass);
			// used to execute queries and such
			Statement test = conn.createStatement();

			int val = test.executeUpdate(folioQuery);
			// 1==updated and 0==not updated
			if (val == 1)
				System.out.println("Folio written.");
			else
				System.out.println("Folio not written.");

			conn.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
	
	public static void writeStock(IPortfolio port, IStock stock) {
		String query = String.format("INSERT INTO stock VALUES('%s', '%s','%s',%d,%f)",
				stock.getTickerSymbol(), port.getName(), stock.getName(), stock.getNumShares(),
				stock.getSharePrice());

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			// connect to the database
			Connection conn = DriverManager.getConnection(hostname, user, pass);
			// used to execute queries and such
			Statement test = conn.createStatement();

			int val = test.executeUpdate(query);
			// 1==updated and 0==not updated
			if (val == 1)
				System.out.println("Stock written.");
			else
				System.out.println("Stock not written.");

			conn.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
	
	public static boolean hasFolio(){
		String query = "SELECT COUNT(*) FROM folio";
		boolean has = false;

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			// connect to the database
			Connection conn = DriverManager.getConnection(hostname, user, pass);
			// used to execute queries and such
			Statement test = conn.createStatement();

			ResultSet result = test.executeQuery(query);
			result.first();
			
			if(result.getInt(1)!=0){
				has= true;
			}

			conn.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return has;
	}

	public static void updateStock(IPortfolio port, IStock stock) {

		String query = String.format(
				"UPDATE stock  SET num_shares=%d WHERE folio='%s' AND name='%s'",
				stock.getNumShares(), port.getName(), stock.getName());

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			// connect to the database
			Connection conn = DriverManager.getConnection(hostname, user, pass);
			// used to execute queries and such
			Statement test = conn.createStatement();

			int val = test.executeUpdate(query);
			// 1==updated and 0==not updated
			if (val == 1)
				System.out.println("Stock written.");
			else
				System.out.println("Stock not written.");

			conn.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	/**
	 * Takes a portfolio and deletes it and any shares it owns from the
	 * database.
	 * 
	 * @param port
	 */
	public static void deleteFolio(IPortfolio port) {
		String folioQuery = String.format("DELETE FROM folio WHERE name='%s'",
				port.getName());
		String sharesQuery = String.format(
				"DELETE FROM stock WHERE folio='%s'", port.getName());

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			// connect to the database
			Connection conn = DriverManager.getConnection(hostname, user, pass);
			// used to execute queries and such
			Statement test = conn.createStatement();

			// shares first so there aren't any foreign key dependency problems
			int val = test.executeUpdate(sharesQuery);
			// 1==updated and 0==not updated
			if (val == 1)
				System.out.println("Shares removed");
			else
				System.out.println("Shares not removed");

			val = test.executeUpdate(folioQuery);
			// 1==updated and 0==not updated
			if (val == 1)
				System.out.println("Folio removed.");
			else
				System.out.println("Folio not removed.");
			conn.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	/**
	 * Deletes a set of stocks from a portfolio.
	 * 
	 * @param port
	 * @param stock
	 */
	public static void deleteStock(IPortfolio port, IStock stock) {
		String query = String.format("DELETE FROM stock WHERE folioName='%s'",
				port.getName());

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			// connect to the database
			Connection conn = DriverManager.getConnection(hostname, user, pass);
			// used to execute queries and such
			Statement test = conn.createStatement();

			// shares first so there aren't any foreign key dependency problems
			int val = test.executeUpdate(query);
			// 1==updated and 0==not updated
			if (val == 1)
				System.out.println("Shares removed");
			else
				System.out.println("Shares not removed");

			conn.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public static Set<IPortfolio> getFolios() {
		String query = "SELECT * FROM folio";
		Set<IPortfolio> folios = new HashSet<IPortfolio>();

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			// connect to the database
			Connection conn = DriverManager.getConnection(hostname, user, pass);
			// used to execute queries and such
			Statement test = conn.createStatement();
			// returns the result of the deletion
			ResultSet results = test.executeQuery(query);
			while(results.next()) {
				folios.add(new Portfolio(results.getString(1)));
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
		folios = getShares(folios);
		
		return folios;
	}

	private static Set<IPortfolio> getShares(Set<IPortfolio> folios) {
		String query = "SELECT * FROM stock WHERE folio='%s'";

		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			// connect to the database
			Connection conn = DriverManager.getConnection(hostname, user, pass);
			// used to execute queries and such
			Statement test = conn.createStatement();
			
			for (IPortfolio folio : folios) {
				
				ResultSet results = test.executeQuery(String.format(query, folio.getName()));
				while(results.next()) {
					String ticker = results.getString(1);
					String folioName = results.getString(3);
					int numShares = results.getInt(4);
					double price = results.getDouble(5);
					folio.addStock(new Stock(ticker, folioName, numShares, price));

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return folios;
	}
}
