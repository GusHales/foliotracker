package Model;
import java.util.HashSet;
import java.util.Set;


public class FolioTracker implements IFolioTracker{
	private Set<IPortfolio> folios;
	
	public FolioTracker(){
		folios = new HashSet<>();
	}
	
	public FolioTracker(Set<IPortfolio> folios){
		this.folios = folios;
	}
	
	public boolean addFolio(String folioName){
		IPortfolio p = new Portfolio(folioName);
		DBManage.writeFolio(p);
		return folios.add(p);
	}
	
	public boolean rmFolio(String folioName){
		for(IPortfolio p : folios){
			if(p.getName().equals(folioName)){
				folios.remove(p);
				DBManage.deleteFolio(p);
				return true;
			}
		}
		return false;
	}
	
	public IPortfolio getPortfolio(String name){
		for(IPortfolio p : folios){
			if(p.getName().equals(name)){
				return p;
			}
		}
		
		return null;
	}
	
	public Set<IPortfolio> getFolios(){
		Set<IPortfolio> temp = folios;
		return temp;
	}
	
	

}
