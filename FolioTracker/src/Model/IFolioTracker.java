package Model;

import java.util.Set;

public interface IFolioTracker {
	public boolean addFolio(String folioName);
	
	public boolean rmFolio(String folioName);
	
	public IPortfolio getPortfolio(String name);
	
	public Set<IPortfolio> getFolios();

}
