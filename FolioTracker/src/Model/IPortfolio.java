package Model;

import java.util.Set;

public interface IPortfolio {
	
	/**
	 * @effects: returns the name of this portfolio
	 */
	public String getName();

	/**
	 * @modifies: this
	 * @effects: if Stocks==null then returns 0
	 * 			 otherwise returns the total value of this portfolio, 
	 * 			 ie. all the value holding of each stock
	 * 			 in this portfolio added together.
	 */
	public double getTotalVal();

	/**
	 * 
	 * @effects: if tickerSymbol == null then throw NullPointerException
	 * 			 else if IStock(tickerSymbol) is not in the set of stocks return null
	 * 			 else return IStock(tickerSymbol) 
	 */
	public IStock getStock(String tickerSymbol);

	
	public boolean addStock(String tickerSymbol);

	public boolean addStock(IStock s);

	public boolean rmStock(String tickerSymbol);

	public int getStockCount();
	
	public Set<IStock> getStocks();
	@Override
	public boolean equals(Object obj);

	@Override
	public int hashCode();
}
