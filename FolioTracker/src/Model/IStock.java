package Model;

import java.io.IOException;

public interface IStock extends IObservable{

		/**
		 * @effects: returns the ticker symbol of this stock
		 * 
		 */
		public String getTickerSymbol();

		/** 
		 * @effects: returns the name of this stock
		 * 
		 */
		public String getName();

		/**
		 * @effects: returns the number of shares of this stock
		 * 
		 */
		public int getNumShares();

		/**
		 * @effects: returns the share price of this stock
		 * 
		 */
		public double getSharePrice();

		/**
		 * @effects: returns the value holding of this stock
		 * 
		 */
		public double getValueHolding();

		/**
		 * @modifies: this
		 * @effects: if num > 0 then this.numShares = this.numShares + num and return true
		 * 			 else num will be negative or 0 so return false
		 */
		public boolean addShares(int num);

		/**
		 * @modifies: this
		 * @effects: if num <= this.numShares then this.numShares = this.numShares - num and return true
		 * 			 else return false
		 */
		public boolean rmShares(int num);

		/**
		 * 
		 * @throws IOException
		 * @throws WebsiteDataException
		 * @throws NoSuchTickerException
		 * @throws MethodException
		 * 
		 * @modifies: this
		 * @effects: obtain a new instance of IQuote so that all changeable variables
		 * 			 are updated (eg. this.sharePrice and this.valHolding)
		 * 			 if this.tickerSymbol == null then throw MethodException
		 */
		public void update() throws IOException, WebsiteDataException, NoSuchTickerException, MethodException;
//
		
		public boolean getIsValid();
//		public void registerObserver(Observer observer);  
//	     public void removeObserver(Observer observer);  
//	     public void notifyObservers(); 


}
