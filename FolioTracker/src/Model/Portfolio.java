package Model;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Set;

public class Portfolio implements IPortfolio{
	public Set<IStock> stocks;
	private String name;
	private double totalVal;

	public Portfolio(String name) {
		this.name = name;
		stocks = new HashSet<IStock>();
		totalVal = 0;

	} 

	/**
	 * @effects: returns the name of this portfolio
	 */
	public String getName() {
		return name;
	}

	/**
	 * @modifies: this
	 * @effects: returns the total value of this portfolio, ie. all the value
	 *           holding of each stock in this portfolio added together.
	 */
	public double getTotalVal() {
		for (IStock s : stocks) {
			totalVal = totalVal + s.getValueHolding();
		}
		return totalVal;
	}

	/**
	 * 
	 * @effects: if tickerSymbol == null then throw NullPointerException else if
	 *           IStock(tickerSymbol) is not in the set of stocks return null
	 *           else return IStock(tickerSymbol)
	 */
	public IStock getStock(String tickerSymbol) {
		IStock st = null;

		if (tickerSymbol == null)
			throw new NullPointerException("tickerSymbol is null");
		else {
			for (IStock s : stocks) {
				if (s.getTickerSymbol().equals(tickerSymbol)) {
					st = s;
				}
			}
		}
		return st;
	}

	/**
	 * 
	 * @param tickerSymbol
	 * @return
	 */
	public boolean addStock(String tickerSymbol) {
		IStock s;
		s = new Stock(tickerSymbol);
		stocks.add(s);
		DBManage.writeStock(this, new Stock(tickerSymbol));
		return true;
	}

	public boolean addStock(IStock s) {
		return stocks.add(s);
	}

	public int getStockCount() {
		return stocks.size();
	}

	public Set<IStock> getStocks() {
		Set<IStock> temp = stocks;
		return temp;
	}

	public boolean rmStock(String tickerSymbol) {
		for (IStock s : stocks) {
			if (s.getTickerSymbol().equals(tickerSymbol)) {
				totalVal = totalVal - s.getValueHolding();
				stocks.remove(s);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Portfolio) {
			Portfolio p = (Portfolio) obj;
			return getName().equals(p.getName());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return getName().hashCode();
	}
}