package Model;

public class SetStockValuesException extends Exception{
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor
	 * 
	 * @param s the exception to be thrown.
	 */
	public SetStockValuesException( String s ) {
		
		super(s);
		
	}//
}
