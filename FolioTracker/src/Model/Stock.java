package Model;

import java.io.IOException;
import java.util.Observable;

import GUI.UpdateStockTable;

public class Stock extends Observable implements IStock {
	private String tickerSymbol, name;
	private int numShares;
	private double sharePrice, valueHolding;
	private IQuote iq;
	private boolean isValid;

	public Stock(String tickerSymbol) {
		iq = new Quote(false);
		this.tickerSymbol = tickerSymbol;
		try {
			iq.setValues(tickerSymbol);
			name = iq.getName();
			numShares = 0;
			setSharePrice();
			calValueHolding();
			isValid = true;
		} catch (IOException | WebsiteDataException | NoSuchTickerException
				| MethodException e) {
			isValid = false;
		}
	}

	public Stock(String tickerSymbol, String name, int numShares, double sharePrice) {
		this.tickerSymbol = tickerSymbol;
		this.name  =  name;
		this.numShares = numShares;
		this.sharePrice = sharePrice;
		calValueHolding();
	}
	
	public boolean getIsValid(){
		return isValid;
	}

	/**
	 * 
	 * @effects: returns the ticker symbol of this stock
	 * 
	 */
	public String getTickerSymbol(){
		return tickerSymbol;
	}

	/**
	 * This method sets the name for this stock. It has been put into its own
	 * method since this will only have to be done once every time a new 
	 * instance of this is created since presumably the name will not change.
	 * 
	 *  @modifies: this
	 *  @effects: if name is null then name is set to "name not found",
	 *  		  else name is set to the value of the stock name obtained
	 *  		  from the server
	 */
//	private void setName(){
//		try {
//			String n = iq.getName();
//			name = n;
//		} catch (MethodException e) {
//			name = "name not found";
//		}
//	}

	/** 
	 * @effects: returns the name of this stock
	 * 
	 */
	public String getName(){
		return name;
	}

	/**
	 * @effects: returns the number of shares of this stock
	 * 
	 */
	public int getNumShares(){
		return numShares;
	}

	/**
	 * @effects: returns the share price of this stock
	 * 
	 */
	public double getSharePrice(){
		return sharePrice;
	}

	/**
	 * This method sets the share price for this stock. It has been put into 
	 * its own method since this will have to be done every time a new instance of 
	 * this is created and also every time that it is updated.
	 * 
	 *  @modifies: this
	 *  @effects: if sharePrice==null then sharePrice = -1
	 *  		  else sharePrice will be set equal to the share value
	 *  		  obtained from the server.
	 */
	private void setSharePrice(){
		try {
			Double p = iq.getLatest();
			sharePrice = p;
		} catch (MethodException e) {
			sharePrice = -1; //a share price will never be negative
							//so we will know an error occurred here.
		}
	}

	/**
	 * This method is used to calculate the total value of the holding
	 * @modifies: this
	 * @effects: valueHolding = number of shares * share price
	 */
	private void calValueHolding(){
		valueHolding = numShares * sharePrice;
	}

	/**
	 * @effects: returns the value holding of this stock
	 * 
	 */
	public double getValueHolding(){
		return valueHolding;
	}

	/**
	 * @modifies: this
	 * @effects: if num > 0 then this.numShares = this.numShares + num and return true
	 * 			 else num will be negative or 0 so return false
	 */
	public boolean addShares(int num){
		if (num > 0){
			numShares = numShares + num;
			calValueHolding();
			System.out.println("HERE " + numShares + " " + valueHolding);
			setChanged();
			notifyObservers();
			System.out.println(numShares);
			System.out.println( "OBS " + this.countObservers());
			return true;
		}
		return false;
	}

	/**
	 * @modifies: this
	 * @effects: if num <= this.numShares then this.numShares = this.numShares - num and return true
	 * 			 else return false
	 */
	public boolean rmShares(int num){
		if(num<=numShares){
			numShares = numShares - num;
			calValueHolding();
			setChanged();
			notifyObservers();
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @throws IOException
	 * @throws WebsiteDataException
	 * @throws NoSuchTickerException
	 * @throws MethodException
	 * 
	 * @modifies: this
	 * @effects: obtain a new instance of IQuote so that all changeable variables
	 * 			 are updated (eg. this.sharePrice and this.valHolding)
	 */
	public void update() throws IOException, WebsiteDataException, NoSuchTickerException, MethodException{
		iq.setValues(tickerSymbol);
		setSharePrice();
		calValueHolding();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof IStock) {
			IStock a = (IStock) obj;
			return a.getTickerSymbol().equals(getTickerSymbol());
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return getTickerSymbol().hashCode();
	}
}

