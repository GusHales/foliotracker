package Model;

public class StockCreationException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Default Constructor
	 * 
	 * @param s: error message
	 */
	public StockCreationException( String s ) {

		super(s);

	}//default constructor

}///:~
