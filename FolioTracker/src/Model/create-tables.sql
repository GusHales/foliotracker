CREATE DATABASE folio_tracker;
USE folio_tracker;

CREATE TABLE folio(
    name VARCHAR(20),
    PRIMARY KEY (name)
);

CREATE TABLE stock(
    ticker VARCHAR(10),
    folio VARCHAR(20),
    name VARCHAR(20),
    num_shares INT, 
    share_price DOUBLE, 
    PRIMARY KEY(ticker, share_price),
    FOREIGN KEY (folio) REFERENCES folio (name)
);
